<?php
require_once "config.php";
if (!empty($_POST)) {
    $validation = new fdb_validation();

    if (empty($_POST['restaurant'])) {
        $validation->formError("Please select a Restaurant.");
    }
    $validation->isInputNotEmpty($_POST["start_time"], "check-in");
    $validation->isInputNotEmpty($_POST["end_time"], "check-out");
    $user_id = $_SESSION["user_id"];

    $validation->validateTime($_POST["start_time"]);
    if (empty($_POST["end_time"])) {

        // checkout vlaidation;
    }
    if (hasNoOpenVisits($user_id)) {
        $visit = new fdb_visits(array(
            "user_id" => $user_id,
            "rest_id" => $_POST["rest_id"],
            "start_time" => $_POST["start_time"],
            "end_time" => $_POST["end_time"],
            "c_count" => $_POST["c_count"],
            "duration" => 0,
            "status" => 'open'
        ));
        $visit->save();
        return $success = true;

        header("refresh:3;location: index.php");
    } else {
        echo "close your visits and come back";
        return  false;
    }
}

function hasNoOpenVisits()
{
    $user_id = $_SESSION["user_id"];
    //check if user has open Visits if yes: it should be closed
    $db = fdb_mysql::get_instance();
    $result = $db->query("SELECT * FROM `visits` WHERE `user_id` = $user_id AND  `status` = 'open';");
    $row = mysqli_fetch_assoc($result);
    if ($row) {
        return false;
    } else {

        return true;
    }
}
