<?php
require_once "config.php";
if (!empty($_POST)) {
    // user login validation
    $validation = new fdb_validation();
    $validation->isInputNotEmpty($_POST["username"], "username");
    $validation->isInputNotEmpty($_POST["password"], "Password");

    if ($validation->formNoErrors()) {
        $db = fdb_mysql::get_instance();

        $sql_username = $db->escape($_POST["username"]);

        $result = $db->query("SELECT * FROM users
          WHERE username = '{$sql_username}' ");
        $user = $result->fetch_assoc();

        if (empty($user) || !password_verify($_POST["password"], $user["password"])) {
            // User not found
            $validation->formError("username or Password was wrong.");
        } else {
            // Remember login in session
            $_SESSION["logged"] = true;
            $_SESSION["user_id"] = $user["id"];
            $_SESSION["username"] = $user["username"];

            header('location: index.php?id=' . $_SESSION["user_id"]);
            exit;
        }
    }
}
