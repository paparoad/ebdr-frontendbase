
<?php
class fdb_validation
{
    private $_errors = array();

    /**
     * proves if a form value is filled.
     * @param string $value input'value
     * @param string $fieledname
     * @return bool false if value is null.
     */
    public function isInputNotEmpty($value, $fieledname)
    {
        if (empty($value)) {
            $this->_errors[] = "Field '{$fieledname}' is empty.";
            echo false;
        }
        return true;
    }

    public function isNotEmptyRestaurantSelctor($value)
    {
        if (empty($value)) {
            echo ("Please select a Restaurant.");
            echo false;

            // echo 'Please select a Restaurant.';
        }
        return true;
    }
    public function formError($msg)
    {
        $this->_errors[] = $msg;
    }

    public function formNoErrors()
    {
        return empty($this->_errors);
    }

    public function errorMsgToHtml()
    {
        if ($this->formNoErrors()) return "";

        $ret = '<ul class="fdb-validation-error">';
        foreach ($this->_errors as $error) {
            $ret .= "<li>{$error}</li>";
        }
        $ret .= "</ul>";
        return $ret;
    }
    public function validateTime($time)
    {
        if ($time == '0000-00-00 00:00:00') {
            $this->formError("check-out time is invalid");
            return false;
        } else return;
    }
    private function checkNotInPast($time)
    {
    }
    private function changeDateTimeFormat($orgDate)
    {
        $newDate = date("Y-m-d-H:i:s", strtotime($orgDate));
        return $newDate;
        //date format : 2021-05-17 02:03:18
    }
    public function getDuration($date1, $date2)
    {
        return abs(($date1 - $date2) / 60); //time in minutes
    }
}
