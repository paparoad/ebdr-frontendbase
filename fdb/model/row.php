<?php
abstract class fdb_model_row
{
    protected $_table; // overwrite in child class
    private $_data;

    public function __construct($data)
    {
        // Check if inherited classes have overwritte the $ _table property
        if (!$this->_table) {
            throw new Exception($data, " Property _table must be overwritten when inheriting from fdb_model_row.");
        }

        if (is_numeric($data)) {
            $result = fdb_mysql::get_instance()->query(
                "SELECT * FROM {$this->_table} WHERE id = ?",
                array($data)
            );
            $data = $result->fetch_assoc();
        }

        $this->_data = $data;
    }

    public function __get($variable)
    {
        return $this->_data[$variable];
    }

    public function save()
    {
        $db = fdb_mysql::get_instance();

        // SQL Field and Value
        $field = "";
        $values = array();
        foreach ($this->_data as $column => $value) {
            if ($column == "id") continue;
            $field .= $column . " = ?, ";
            $values[] = $value;
        }
        $field = trim($field, ", ");

        if (!empty($this->_data["id"])) {
            $values[] = $this->_data["id"];
            $db->query("UPDATE {$this->_table} SET {$field} WHERE id = ?", $values);
        } else {
            $db->query("INSERT INTO {$this->_table} SET {$field}", $values);
        }
    }

    public function delete()
    {
        fdb_mysql::get_instance()->query(
            "DELETE FROM {$this->_table} WHERE id = ?",
            array($this->_data["id"])
        );
    }
}
