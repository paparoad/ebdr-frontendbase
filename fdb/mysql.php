<?php
class fdb_mysql
{
    // Singleton Implementation
    // Avoids multiple creation of the same object.

    private static $_instance;

    public static function get_instance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }



    // database connection
    private $_db;

    private function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        // Do not make a new connection if one already exists
        if ($this->_db) return;
        // Establish a connection to MySQL database
        $this->_db = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);
        $this->_db->set_charset("utf8");
    }

    public function escape($value)
    {
        return $this->_db->real_escape_string($value);
    }

    public function query($sql_command, array $placeholder = array())
    {
        // Prepare SQL command in statement object
        $stmt = $this->_db->prepare($sql_command);
        if (!$stmt) {
            echo $sql_command . "<br />";
            die($this->_db->error);
        }
        // include placeholder
        if ($placeholder) {
            $stmt->bind_param(
                str_repeat("s", count($placeholder)),
                ...$placeholder
            );
        }
        // Execute the query, get the result and close the statement
        if (!$stmt->execute()) {
            echo $sql_command . "<br />";
            die($this->_db->error);
        }
        $result = $stmt->get_result();
        $stmt->close();

        return $result;
    }
}
