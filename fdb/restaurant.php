<?php
class fdb_restaurant
{
  private $_id;
  private $_url;
  private $_name;
  private $_location;
  private $_max_guests;
  private $_actuall_guests;
  private $_free_seats;



  function getRestaurants()
  {
    $db = fdb_mysql::get_instance();
    return $res=  $db->query("SELECT * FROM `restaurants` WHERE `freeSeats` > 0");
  }

  public function set_actuall_guests($c_countVisit, $marke_id)
  {
    $this->_actuall_guests = $c_countVisit + 1;
    $this->update_free_seats();
  }

  private function update_free_seats()
  {
      $this->_free_seats =   
      $this->_max_guests - $this->_actuall_guests;
  }

  public function getNameByID($id)
  {
    $db = fdb_mysql::get_instance();
    $res = $db->query("SELECT `name` FROM `restaurants` WHERE `id` = $id");
    $row = mysqli_fetch_assoc($res);
    return strval(($row['name']));
  }

}
