<?php
require_once "config.php";
include "./includes/logic/createVisit.php";
include("./includes/layouts/header.php");
is_logged_in();
$restaurants = new fdb_restaurant();
$rest_name = $restaurants->getRestaurants();
$success = false;

if ($success) {
    echo '<div class="alert alert-success">Thank You! you checked-in... now remember you can always check out.</div>';
} else {
    $echo = '<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
}

?>

<div class="wrapper" style="  max-width: 960px; /* 20px smaller, to fit the paddings on the sides */
  padding-right: 10px;
  padding-left: 10px;">
    <form method="post" action="visit.php<?php echo "?$_SESSION[user_id]"; ?>">
        <div class="form-group">
            <label for="restaurant ">Select a Restaurant</label>
            <select required class="form-control restaurant" name="rest_id">
                <option value="" disabled selected>---plese select a Restaurant---</option>
                <?php
                while ($row = mysqli_fetch_assoc($rest_name)) {
                    echo "<option value=" . $row['id'] . ">" . $row['name'] . "</option>";
                }
                ?>
            </select>
        </div>
        <div class=" form-group">
            <label for="start_time">Check-in</label>
            <input name="start_time" type="datetime-local" class="form-control" id="start_time">
        </div>
        <div class=" form-group">
            <label for="end_time">Check-out</label><br>
            <small>You can check out any time you like
                But you can never leave!</small>
            <input name="end_time" type="datetime-local" class="form-control" id="end_time">
            <!-- 0000-00-00 00:00:00 -->
        </div>
        <div class=" form-group">
            <label for="c_count">Number of Compininon</label>
            <input value="0" max="200" min="0" name="c_count" type="number" class="form-control" id="c_count" placeholder="How many Visitors are comming with you?">
        </div>
        <button type="submit" name="visit_restaurant" class="btn btn-primary">Visit</button>
        <br><br>

    </form>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <!-- <?php if (hasNoOpenVisits() && !empty($visit)) {
                        // getsuccess();
                    } ?> -->
        </div>
    </div>
    <?php
    if (!empty($validation)) {
        echo $validation->errorMsgToHtml();
    }
    ?>