<?php include('server.php') ?>
<!DOCTYPE html>
<html lang="de">

<head>
  <title>Register | EBDR</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="assets/css/style.css">

</head>


<body>
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><img src="assets/images/logo.png" alt="Erfassung der Besucherdaten
                 eines Restaurants"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="register.php">Register</a></li>
          <li><a href="login.php">Login</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="wrapper">
    <h2 class="header">Sign Up</h2>

    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
      <div class="form-group">
        <label for="username">Username</label>
        <input type="text" name="username" id="name" class="form-control" placeholder="Enter username" value="<?php echo $username; ?>">
      </div>

      <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" name="email" id="email" placeholder="Enter email" class="form-control" value="<?php echo $email; ?>">
      </div>



      <div class="form-group">
        <label for="password_1">Password</label>
        <input name="password_1" type="password" class="form-control" id="password_1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="password_2">Confirm Password</label>
        <input name="password_2" type="password" class="form-control" id="password_2" placeholder="Confirm Password">
      </div>
      <button type="submit" class="btn btn-primary" name="reg_user">Register</button>
      <br><br>
      <p>Already have an account? <a href="login.php">Login here</a>.</p>

    </form>
  </div>
</body>

</html>