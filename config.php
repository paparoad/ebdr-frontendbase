<?php
// Project's Configuration
define("MYSQL_HOST", "localhost");
define("MYSQL_USER", "root");
define("MYSQL_PASSWORD", "");
define("MYSQL_DATABASE", "ebdr_jwe21");

// Setup-Code
session_start();

spl_autoload_register(function ($class) {
    $link = str_replace("_", "/", $class);
    require_once $link . ".php";
});

function is_logged_in()
{
    if (empty($_SESSION["logged"])) {
        header("Location: login.php");
        exit;
    }
}
