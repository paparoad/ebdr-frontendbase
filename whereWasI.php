<?php
require_once "config.php";
include "./includes/layouts/header.php";
include "./includes/logic/createVisit.php";

is_logged_in() ?>

<?php
    if (!hasNoOpenVisits()) {
        echo ' <h2>Where Was I[Visit Archive]</h2><br><br>';
        include("./includes/logic/getHistory.php");
    }
    ?>