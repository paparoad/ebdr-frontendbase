<?php
require_once "config.php";
include "./includes/layouts/header.php";
// include "./includes/layouts/userDashboard.php";
include "./includes/logic/createVisit.php";

is_logged_in() ?>
<div class="product-range">
    <?php
    if (!hasNoOpenVisits()) {
        echo ' <h2>Currently you have those open Visits, please Close them </h2><br><br>';
        include("./includes/logic/editVisit.php");
    }
    ?>

    <div class="four-colums">
        <div><?
                ?></div>
        <div class="item">
            <div class="item-inner">

                <a href="visit.php<?php echo "?$_SESSION[user_id]"; ?>">
                    <img src="assets/images/checkin.jpg" alt="check-in a restaurant"></a>
                <div class="overlay">
                    <div class="inner">
                        <p>Check-in</p>

                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <div class="item-inner">
                <a href="whereWasI.php<?php echo "?$_SESSION[user_id]"; ?>">
                    <img src="assets/images/history.png" alt="history"></a>
                <div class="overlay">
                    <div class="inner">
                        <p>History</p>
                    </div>
                </div>
            </div>
        </div>

    </div>


    </body>

    </html>