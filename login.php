<?php
require_once "config.php";
require_once "./includes/logic/userLogin.php";
include("./includes/layouts/loginHeader.php")
?>



<div class="wrapper">
    <h2 class="header">Sign In</h2>
    <form method="post" action="login.php">
        <div class="form-group">
            <label for="username">Username</label>
            <input name="username" type="text" class="form-control" id="username" placeholder="Enter username">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password">
        </div>
        <button type="submit" name="login_user" class="btn btn-primary">Login</button>
        <br><br>
        <?php
        if (!empty($validation)) {
            echo $validation->errorMsgToHtml();
        }
        ?>
        <p>Don't have an account Yet? <a href="register.php">Sign up</a>.</p>

        <!-- <p>Click the button to get your coordinates.</p> -->

        <p id="demo"></p>

        <script>
            var x = document.getElementById("demo");

            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }

            function showPosition(position) {
                var lat = position.coords.latitude;
                var lon = position.coords.longitude;
            }
        </script>
    </form>
</div>
</body>

</html>