<?php
require_once "config.php";
// require_once "./includes/logic/editVisit.php";
// require_once "./includes/logic/createVisit.php";

include("./includes/layouts/header.php");
is_logged_in();
$success = false;

$restaurants = new fdb_restaurant();
$rest_name="";

$rest_name= $restaurants->getNameByID($_GET["rID"]);
// echo $test;

// success msg
if ($success) {
    echo "<p>Visit updated.<br />
    <a href=\"index.php\">back home</a>.
  </p>";
}
// form error 
if (!empty($validation)) {
    echo $validation->errorMsgToHtml();
}
if (!empty($_GET["id"])) {
    $row = new fdb_visits($_GET["id"]);
}
?>
<div class="wrapper">
    <h2 class="header">Edit visit</h2>

    <form action="edit_visit.php<?php
                                if (!empty($_GET["id"])) echo "?id=" . $_GET["id"] . "&rD=" .  $_GET["rd"];
                                ?>" method="post">
        <div class="form-group">
            <label for="restaurant">Restaurant</label>
            <input readonly="readonly" class="form-control" type="text" name="restaurant" id="restaurant" value="<?php
            echo htmlspecialchars($rest_name);
            ?>">
        </div>
        <div class="form-group">
            <label for="start_time">Checked-in @</label>
            <input readonly="readonly" name="start_time" type="text" class="form-control" id="start_time" value="<?php if (!$success && !empty($_POST["start_time"])) {
                                                                                                                        echo htmlspecialchars($_POST["start_time"]);
                                                                                                                    } else if (!empty($row)) {
                                                                                                                        echo htmlspecialchars($row->start_time);
                                                                                                                    }
                                                                                                                    ?>">

            <?php

            ?>
        </div>
        <div class="form-group">
            <label for="end_time">Checked-out @</label>
            <input  name="end_time" type="datetime-local" class="form-control" id="end_time" value="<?php if (!$success && !empty($_POST["end_time"])) {
                                                                                                                echo htmlspecialchars($_POST["end_time"]);
                                                                                                            } else if (!empty($row)) {
                                                                                                                if ($row->end_time == "0000-00-00 00:00:00
                                                                                                        ") {
                                                                                                                    echo htmlspecialchars("");
                                                                                                                };
                                                                                                            }
                                                                                                            ?>">


        </div>

        <div class="form-group">
            <label for="c_count">Number of Compinion</label>
            <input readonly="readonly" min="0" max="10" name="c_count" type="number" class="form-control" id="c_count" value="<?php if (!$success && !empty($_POST["c_count"])) {
                                                                                                                echo htmlspecialchars($_POST["c_count"]);
                                                                                                            } else if (!empty($row)) {
                                                                                                                echo htmlspecialchars($row->c_count);
                                                                                                            }
                                                                                                            ?>
">

        </div>
        <div class="form-group">
            <label for="status">Vist's Status</label><br>
            <small>Visit will be closed Automaticlly after checking out</small>
            <input readonly="readonly" name="status" type="text" class="form-control" id="status" value="<?php if (!$success && !empty($_POST["status"])) {
                                                                                                                echo htmlspecialchars($_POST["status"]);
                                                                                                            } else if (!empty($row)) {
                                                                                                                echo htmlspecialchars($row->status);
                                                                                                            }
                                                                                                            ?>">

        </div>

        <div>
            <button class="btn btn-primary" type="submit">Save</button>
        </div>
        <div><?php
                if (!empty($validation)) {
                    echo $validation->errorMsgToHtml();
                }
                ?></div>
    </form>
</div>